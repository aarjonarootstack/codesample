<?php

abstract class BaseUserMigration extends DrupalUser7Migration {
  public function __construct($arguments) {
    // additions to sourceFields must go before running the parent constructor
    $this->sourceFields['roles'] = t('User Roles');

    parent::__construct($arguments);

    $this->team = array(
      new MigrateTeamMember('Ariel Arjona', 'arielarjona+migrate@appnovation.com',
        t('Product Owner')),
    );

    $this->addSimpleMappings(array(
      'field_user_first_name',
      'field_user_first_name:language',
      'field_user_last_name',
      'field_user_last_name:language',
      'field_phone_number',
      'field_user_about',
      'field_user_about:summary',
      'field_user_about:format',
      'field_user_about:language',
      'field_company_name',
      'field_company_name:language',
      'field_organisation_type',
      'field_user_access_request',
      'field_do_you_wish_to_receive_new',
      'field_if_other',
      'field_if_other:language',
    ));

    $this->addFieldMapping('field_display_name', 'field_user_display_name');
    $this->addFieldMapping('field_display_name:language', 'field_user_display_name:language');

    $this->addFieldMapping('field_user_picture', 'field_user_picture')
      ->sourceMigration('LccUserPicture');

    $this->addFieldMapping('field_user_picture:file_class')
      ->defaultValue('MigrateFileFid');
    $this->addFieldMapping('field_user_picture:preserve_files')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_user_picture:title', 'field_user_picture:title')
      ->defaultValue(t('LOWC Picture'));
    $this->addFieldMapping('field_user_picture:alt', 'field_user_picture:alt')
      ->defaultValue('');
    $this->addFieldMapping('field_user_picture:language', 'field_user_picture:language')
      ->defaultValue(LANGUAGE_NONE);

    // TODO: check whether these language fields should appear in destination
    $this->addUnmigratedSources(array(
      'field_phone_number:language',
      'field_organisation_type:language',
      'field_user_access_request:language',
      'field_do_you_wish_to_receive_new:language',
    ));


  }

  /**
   * Used to test migrations against a testing uid defined in the migrate api
   *
   * @return \QueryConditionInterface
   */
  protected function query() {
    $query = parent::query();
    $tuid = $this->getTestingUid();
    if ($tuid !== NULL) {
      $query->condition('u.uid', $tuid);
    }
    return $query;
  }

  /**
   * Look up the OG membership data and map it manually to the dest nodes
   *
   * @param $node
   * @param $row
   */
  public function prepare($node, $row) {

    $groupmap = $this->getGroupMap();

    // get the og membership array for the user
    $conn = Database::getConnection('default', $this->sourceConnection);
    $query = $conn->select('og_membership', 'ogm');
    $query->condition('ogm.etid', $row->uid);
    $query->condition('ogm.entity_type', 'user');
    $query->condition('ogm.field_name', 'og_user_node');
    $query->condition('ogm.state', 1);
    $query->fields('ogm', array('gid'));
    $result = $query->execute()->fetchCol();

    foreach ($result as $r) {
      if (array_key_exists($r, $groupmap)) {
        $uuid = $groupmap[$r];
        $nid = entity_get_id_by_uuid('node', array($uuid))[$uuid];
        $node->og_user_node[LANGUAGE_NONE][] = array('target_id' => $nid);
      }
    }
  }

  abstract function getGroupMap();
  abstract function getTestingUid();

}

class LccUserMigration extends BaseUserMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->description = t('Migrate users from the LCC site');
  }

  /**
   * Array keys are source nids, values are destination uuids
   *
   * @return array
   */
  public function getGroupMap() {
    $groupmap = array(
      11 => '5733e18c-9755-4bf2-9454-b3be10c368cd', // suppliers
      15 => '1006a863-4aee-4aad-80f5-bd7f27be534f', // generators
      100 => 'abcedfec-481f-4706-9233-ae7ab66a7d22', // media
    );
    return $groupmap;
  }

  public function getTestingUid() {
    return 647;
  }
}

class LccUserPictureMigration extends DrupalPicture7Migration {
  public function __construct($arguments) {
    $this->sourceFields['urlencode'] = t('urlencoding of the path');
    parent::__construct($arguments);
  }
}

class EscUserMigration extends BaseUserMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->description = t('Migrate users from the ESC site');
  }

  /**
   * Array keys are source nids, values are destination uuids
   *
   * @return array
   */
  public function getGroupMap() {
    $groupmap = array(
      9 => '5733e18c-9755-4bf2-9454-b3be10c368cd', // suppliers
      15 => '1006a863-4aee-4aad-80f5-bd7f27be534f', // generators
    );
    return $groupmap;
  }

  public function getTestingUid() {
    return 71;
  }
}

class EscUserPictureMigration extends DrupalPicture7Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);
  }
}
